#keywords
stress_list = ['stress', 'stress.' 'stressed', 'stressed.', 'stressing', 'stressing.', 'stressful', 'stressful.']
slow_list = ['slow', 'slow.', 'sluggish', 'sluggish.', 'slug', 'slug.', 'slowly', 'slowly.', 'slowing','slowing.', 'slowed', 'slowed.', 'slower', 'slower.']
tired_list = ['tire', 'tire.', 'tired','tired.', 'tiring', 'tiring.']

#feelings list
feelings_list = stress_list + slow_list + tired_list

#check word function
def check_word(user_feelings):
    for word in user_feelings:
        if word in feelings_list:
            return word

#response handler function
def response_handler(found_word):
    if found_word in stress_list:
        return ('If you are stressed, take a moment, breathe. Make an action plan and give yourself an easy win. This will help ease some of the stress.')
    elif found_word in slow_list:
        return ('If you are slow today, it is okay. Stay steady and keep doing what you need to do.')
    elif found_word in tired_list:
        return ('If you are tired, splash your face with some water. Or spray your fave face mist! You will feel fresher.')
    else:
        return ('I am sorry, I do not know this feeling yet. Would you like to share another feeling with me?')

#chatbot setup
def feelingsbot_logic():
    feelingsbot_intro = print('Hi, I am feelingsbot. Share your feelings with me and I will respond with advise or affirmation. If you no longer wish to speak with me, say bye!\n')
    user_response = input('How are you feeling?\n')

    while 'bye' not in user_response:
        user_feelings = user_response.lower().split()
        print(user_feelings)
        
        found_word = check_word(user_feelings)
        print(found_word)

        response = response_handler(found_word)
        print(response)

        user_response = input('How are you feeling?\n')

feelingsbot_logic() #ok ash i copied ur code here, what does this actually do lol, does it allow it to loop back to the chatbot set up?
